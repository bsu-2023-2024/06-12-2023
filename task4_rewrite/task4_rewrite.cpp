#include <iostream> 

using namespace std;

char ToUpper(char);
bool IsAlfa(char symbol);
bool Equals(char lhs[], char rhs[], bool caseSensetive);
int Compare(char lhs[], char rhs[], bool caseSensetive);

int main()
{
    const int maxLength = 100;
    char lhs[maxLength]{};
    cout << "Enter some string, less than " << maxLength << " symbols: ";
    cin.getline(lhs, maxLength);

    char rhs[maxLength]{};
    cout << "Enter some string, less than " << maxLength << " symbols: ";
    cin.getline(rhs, maxLength);

    bool caseSensetive = false;
    cout << "Enter 1 or 0 if we consider case Sensetive: ";
    cin >> caseSensetive;

    int result = Compare(lhs, rhs, caseSensetive);
    cout << result;
}

char ToUpper(char symbol)
{
    return symbol <= 'z' && symbol >= 'a' ? symbol - 32 : symbol;
}

bool IsAlfa(char symbol)
{
    return symbol >= 'A' && symbol <= 'Z' || symbol >= 'a' && symbol <= 'z';
}

bool Equals(char lhs[], char rhs[], bool caseSensetive)
{
    if (strlen(lhs) != strlen(rhs))
    {
        return false;
    }

    for (int i = 0; lhs[i]; i++)
    {
        char lhSymbol = caseSensetive ? lhs[i] : ToUpper(lhs[i]);
        char rhSymbol = caseSensetive ? rhs[i] : ToUpper(rhs[i]);

        if (lhSymbol != rhSymbol)
        {
            return false;
        }
    }
    return true;
}

int Compare(char lhs[], char rhs[], bool caseSensetive)
{
    size_t length = strlen(lhs) <= strlen(rhs) ? strlen(lhs) : strlen(rhs);

    for (size_t i = 0; length; i++)
    {
        char lhSymbol = caseSensetive ? lhs[i] : ToUpper(lhs[i]);
        char rhSymbol = caseSensetive ? rhs[i] : ToUpper(rhs[i]);

        if (lhSymbol - rhSymbol > 0)
        {
            return 1;
        }
        else if (lhSymbol - rhSymbol < 0)
        {
            return -1;
        }
    }

    if (strlen(lhs) - strlen(rhs) == 0)
    {
        return 0;
    }
    else if (strlen(lhs) - strlen(rhs) > 0)
    {
        return 1;
    }
    else
    {
        return -1;
    }

}