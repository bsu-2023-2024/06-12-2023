#include <iostream>

using namespace std;

void getUserString(char arr[], int);
bool isEqualStrings(char arr1[], char arr2[]);

int main()
{
    const int maxLength = 100;
    char text1[maxLength]{};
    getUserString(text1, maxLength);
    char text2[maxLength]{};
    getUserString(text2, maxLength);
    if (isEqualStrings(text1, text2))
    {
        cout << "Strings are equal";
    }
    else
    {
        cout << "Strings are not equal";
    }

}

void getUserString(char arr[], int maxLength)
{
    cout << "Enter some text, less than " << maxLength << " symbols: ";
    cin.getline(arr, maxLength);
}

bool isEqualStrings(char arr1[], char arr2[])
{
    for (size_t i = 0; arr1[i] || arr2[i]; i++)
    {
        if (arr1[i] != arr2[i])
        {
            return false;
        }
    }
    return true;
}